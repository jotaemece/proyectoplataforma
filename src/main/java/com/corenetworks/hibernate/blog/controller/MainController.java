package com.corenetworks.hibernate.blog.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.corenetworks.hibernate.blog.dao.PreguntaDao;
import com.corenetworks.hibernate.blog.dao.ProfesorDao;

@Controller
public class MainController {
	
	@Autowired
	private PreguntaDao preguntaDao;
	
	@GetMapping(value="/")
	public String welcome(Model modelo) {
		
		modelo.addAttribute("listaPregunta", preguntaDao.getAll());
		return "index";
		
	}
}
