<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c"  %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix = "fn"  uri = "http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>   
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="author" content="Core Netowkrs Sevilla">
<meta name="description" content="Blog creado con Spring Boot, Spring MVC, JPA e Hibernate">

<title>CoreQA</title>

<link href="webjars/bootstrap/3.3.7-1/css/bootstrap.min.css"  rel="stylesheet">
<link href="assets/css/jumbotron-narrow.css"  rel="stylesheet">
</head>
<body>
  <div class="container">
    <div class="header clearfix">
	    <nav>
	     <ul  class="nav nav-pills pull-right">
		       <c:choose>
		         <c:when test="${not empty sessionScope.userLoggedIn }">
		            <jsp:include page="includes/menu_logged.jsp">
		            <jsp:param value="login" name="login"/>
		            <jsp:param name="usuario" value="${sessionScope.userLoggedIn.nombre }"/>
		            
		          </jsp:include>
		         </c:when>
		         <c:otherwise>
		          <jsp:include page="includes/menu.jsp">
		            <jsp:param value="login" name="login"/>
		          </jsp:include>
		         </c:otherwise> 
		       </c:choose>   
		       </ul>    
	    </nav>
	    <h3 class="text-muted">CoreQA</h3>
    
    </div>
    <div class="row">
    
	    <div class="col-lg-8 col-lg-offset-2" >
	    
	       <div class="text-center">
	          <h3>Formulario de acceso</h3>
	          <form:form id="login-form"  action="/login" method="post" role="form" 
	             modelAttribute="profesorLogin"
	          >
	           <div class="form-group">
	              <form:input type="email" name="email" id="email" tabIndex="1" class="form-control"   path="email" placeholder="Email" required="required"
	                     autofocus="autofocus" />
	           </div>
	           <div class="form-group">
	              <form:input type="password" name="password" id="password" tabIndex="2" class="form-control"   path="password" placeholder="Password"  required="required" />
	           </div>
	            <div class="form-group">
	              <button type="submit" name="login-submit" id="login-submit" tabIndex="3" class="btn btn-lg btn-primary btn-block" >Login</button>               
	           </div>          
	          </form:form>
	       </div>    
	    </div>
    </div>
    
      <c:if test="${not empty error}">
    <div class="row">
    
	      <div class="col-lg-8 col-lg-offset-2" >
	         <div class="alert alert-danger alert-dismissible fade in" role="alert">
	         <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	         <!--  Esto hace que se muestre una X en la zona de error  -->
	         <%  
	                // Esto hace que se muestre una X en la zona de error  %>
	          <%    /* Esto hace que se muestre una X en la zona de error 
	                        Varias líneas
	          
	                 */%>
	         
	            <span aria-hidden="true">&times;</span>
	         </button>
                 <c:out value="${error}"></c:out>
              </div>
         </div>
       </div>  
      </c:if>
    <footer class="footer">
        
      
    </footer>

  
  
  </div>
  
  
  <script src="webjars/jquery/3.1.1/jquery.min.js"></script> 
  <script src="webjars/bootstrap/3.3.7-1/js/bootstrap.min.js"></script>
  <script src="assets/js/jquery.validate.min.js"></script>
  <script src="assets/js/messages_es.js"></script>
  <script>
    $(document).ready(function() { 
              $("#register-form").validate();
         }
     );
     
  </script>
</body>
</html>